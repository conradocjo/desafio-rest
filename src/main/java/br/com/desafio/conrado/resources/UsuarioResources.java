package br.com.desafio.conrado.resources;

import static br.com.desafio.conrado.utils.ResourcesUtils.USUARIO_A_SER_DELETADO_NAO_ENCONTRADO;
import static br.com.desafio.conrado.utils.ResourcesUtils.USUARIO_BUSCADO_NAO_ENCONTRADO;
import static br.com.desafio.conrado.utils.ResourcesUtils.USUARIO_A_SER_ATUALIZADO_NAO_ENCONTRADO;
import static java.util.Objects.nonNull;

import java.io.IOException;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.desafio.conrado.exceptions.ObjectNotFound;
import br.com.desafio.conrado.model.DigitoUnico;
import br.com.desafio.conrado.model.Usuario;
import br.com.desafio.conrado.model.dto.Keys;
import br.com.desafio.conrado.model.dto.ParametrosCalculoDTO;
import br.com.desafio.conrado.services.DigitoUnicoService;
import br.com.desafio.conrado.services.RSAGeneratorService;
import br.com.desafio.conrado.services.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "API Rest Desafio")
@RestController
@RequestMapping("/usuarios")
public class UsuarioResources {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private DigitoUnicoService digitoUnicoService;

	@Autowired
	private RSAGeneratorService rsaGeneratorService;

	/**
	 * EndPoint para calculo do dígito, este cálculo pode ser associado de forma
	 * opcional a um usuário
	 */
	@ApiOperation(value = "EndPoint para calculo do dígito, este cálculo, pode ser associado de forma opcional a um usuário")
	@PostMapping(value = "/calculaDigitoUnico")
	public void realizaCalculoDoDigito(@RequestBody ParametrosCalculoDTO parametro) {
		List<DigitoUnico> listaDigitoUnicos = new ArrayList<>();
		listaDigitoUnicos.add(new DigitoUnico(
				this.usuarioService.retornaValorDigitoUnico(parametro.getPrimeiroParametro().toString(),
						parametro.getSegundoParametro().toString()),
				parametro.getPrimeiroParametro(), parametro.getSegundoParametro()));

		if (nonNull(parametro.getIdUsuario())) {
			Optional<Usuario> usuario = this.usuarioService.retornaUsuario(parametro.getIdUsuario());
			for (DigitoUnico digito : listaDigitoUnicos) {
				digito.setUsuario(usuario.get());
			}
		}
		this.digitoUnicoService.salvarCalculoDigitoUnico(listaDigitoUnicos);
	}

	/**
	 * EndPoint recupera todos os cálculos para um determinado usuário
	 */
	@ApiOperation(value = "Recupera todos os cálculos para um determinado usuário")
	@GetMapping(value = "/recuperaCalculoDoUsuario/{idUsuario}")
	public ResponseEntity<List<DigitoUnico>> recuperaTodosCalculosParaUmDeterminadoUsuario(
			@PathVariable Long idUsuario) {
		return ResponseEntity.ok().body(this.digitoUnicoService.buscarTodosCalculosDoUsuario(idUsuario));
	}

	/**
	 * endPoint retorna chave Publica.
	 */
	@ApiOperation(value = "Gera chave privada e publica.")
	@GetMapping(value = "/gerarChaves/{nome}/{email}")
	public ResponseEntity<Keys> gerarChaves(@PathVariable String nome, @PathVariable String email) {
		Keys chaves = null;
		try {
			Usuario usuario = new Usuario(nome, email);
			Map<Usuario, Object> keys = this.rsaGeneratorService.generatePublicAndPrivateKeys(usuario);
			PublicKey chavePublica = ((KeyPair) keys.get(usuario)).getPublic();
			chaves = new Keys(chavePublica.toString(), null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok().body(chaves);
	}

	/**
	 * EndPoint para encriptar usuario
	 */
	@ApiOperation(value = "Criptografa os dados do usuário.")
	@GetMapping("/criptografarDadosDoUsuario/{nome}/{email}")
	public ResponseEntity<Usuario> encriptarDadosDoUsuario(@PathVariable String nome, @PathVariable String email) {
		Usuario usuario = new Usuario(nome, email);
		try {
			usuario = new Usuario(nome, email);
			Map<Usuario, Object> keys = this.rsaGeneratorService.generatePublicAndPrivateKeys(usuario);
			PublicKey chavePublica = ((KeyPair) keys.get(usuario)).getPublic();
			usuario = this.rsaGeneratorService.encryptUser(usuario, chavePublica);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok().body(usuario);
	}

	
	// EndPoints do CRUD de usuários

	/**
	 * EndPoint para gravar usuário
	 */
	@ApiOperation(value = "Realiza cadastro de usuários.")
	@PostMapping("/cadastrarUsuario")
	public ResponseEntity<Void> gravarUsuario(@RequestBody Usuario usuario) {
		Usuario retorno = this.usuarioService.gravarUsuario(usuario);
		URI uri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/usuarios/listarUsuario/{id}")
				.buildAndExpand(retorno.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	/**
	 * Recurso para atualizar usuário existente.
	 */
	@ApiOperation(value = "Atualiza cadastro de usuário.")
	@PutMapping("/atualizar")
	public void atualizarUsuario(@RequestBody Usuario usuario) {
		Usuario usuarioBuscadoDoBancoDeDados = this.usuarioService.retornaUsuario(usuario.getId())
				.orElseThrow(() -> new ObjectNotFound(USUARIO_A_SER_ATUALIZADO_NAO_ENCONTRADO));
		if (nonNull(usuarioBuscadoDoBancoDeDados)) {
			this.usuarioService.atualizarUsuario(usuarioBuscadoDoBancoDeDados, usuario);
		}
	}

	/**
	 * Recurso para deletar usuário por id.
	 */
	@ApiOperation(value = "Deleta usuario.")
	@DeleteMapping("deletar/{id}")
	public void deletarUsuario(@PathVariable Long id) {
		Usuario usuario = this.usuarioService.retornaUsuario(id)
				.orElseThrow(() -> new ObjectNotFound(USUARIO_A_SER_DELETADO_NAO_ENCONTRADO));
		this.usuarioService.deletarUsuario(usuario);
	}

	/**
	 * Recurso para listar todos usuários
	 */
	@ApiOperation(value = "Lista todos usuários.")
	@GetMapping("/listarUsuarios")
	public ResponseEntity<List<Usuario>> retornarUsuarios() {
		return ResponseEntity.ok().body(this.usuarioService.retornarUsuarios());
	}

	/**
	 * Recurso para busca de usuário por ID
	 */
	@ApiOperation(value = "Busca usuário pelo ID.")
	@GetMapping("/listarUsuario/{id}")
	public ResponseEntity<Usuario> retornarUsuario(@PathVariable Long id) {
		return ResponseEntity.ok(this.usuarioService.retornaUsuario(id)
				.orElseThrow(() -> new ObjectNotFound(USUARIO_BUSCADO_NAO_ENCONTRADO)));
	}

}
