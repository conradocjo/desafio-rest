package br.com.desafio.conrado.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "TB_DIGITO_UNICO")
public class DigitoUnico implements Serializable {

	private static final long serialVersionUID = 1919656322624284612L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "digito_unico")
	private Integer digitoUnico;

	@Column(name = "primeiro_parametro")
	private Integer primeiroParametro;

	@Column(name = "segundo_parametro")
	private Integer segundoParametro;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "usuario")
	private Usuario usuario;

	public DigitoUnico(){
		
	}
	public DigitoUnico (Integer digitoUnico, Integer primeiroParametro, Integer segundoParametro) {
		this.digitoUnico = digitoUnico;
		this.primeiroParametro = primeiroParametro;
		this.segundoParametro = segundoParametro;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDigitoUnico() {
		return digitoUnico;
	}

	public void setDigitoUnico(Integer digitoUnico) {
		this.digitoUnico = digitoUnico;
	}

	public Integer getPrimeiroParametro() {
		return primeiroParametro;
	}

	public void setPrimeiroParametro(Integer primeiroParametro) {
		this.primeiroParametro = primeiroParametro;
	}

	public Integer getSegundoParametro() {
		return segundoParametro;
	}

	public void setSegundoParametro(Integer segundoParametro) {
		this.segundoParametro = segundoParametro;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
