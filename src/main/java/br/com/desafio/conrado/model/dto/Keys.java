package br.com.desafio.conrado.model.dto;

import java.io.Serializable;

public class Keys implements Serializable {

	private static final long serialVersionUID = -1512709891883665238L;

	private String chavePublica;

	private String chavePrivada;

	public Keys(String chavePublica, String chavePrivada) {
		this.setChavePublica(chavePublica);
		this.setChavePrivada(chavePrivada);
	}

	public Keys() {

	}

	public String getChavePublica() {
		return chavePublica;
	}

	public void setChavePublica(String chavePublica) {
		this.chavePublica = chavePublica;
	}

	public String getChavePrivada() {
		return chavePrivada;
	}

	public void setChavePrivada(String chavePrivada) {
		this.chavePrivada = chavePrivada;
	}

}
