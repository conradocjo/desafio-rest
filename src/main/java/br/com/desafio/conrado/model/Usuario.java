
package br.com.desafio.conrado.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TB_USUARIO")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 4668725775383285928L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nome", nullable = false)
	private String nome;

	@Column(name = "email", nullable = false, length = 40)
	private String email;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario",fetch = FetchType.LAZY)
	private List<DigitoUnico> listaResultadosDigitosUnicosCalculados;
	

	public Usuario(String nome, String email) {
		this.nome = nome;
		this.email = email;
	}

	public List<DigitoUnico> getListaResultadosDigitosUnicosCalculados() {
		return listaResultadosDigitosUnicosCalculados;
	}

	public void setListaResultadosDigitosUnicosCalculados(List<DigitoUnico> listaResultadosDigitosUnicosCalculados) {
		this.listaResultadosDigitosUnicosCalculados = listaResultadosDigitosUnicosCalculados;
	}

	public Usuario() {
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
