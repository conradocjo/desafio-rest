package br.com.desafio.conrado.model.dto;

public class ParametrosCalculoDTO {

	private Integer primeiroParametro;

	private Integer segundoParametro;

	private Long idUsuario;


	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Integer getPrimeiroParametro() {
		return primeiroParametro;
	}

	public void setPrimeiroParametro(Integer primeiroParametro) {
		this.primeiroParametro = primeiroParametro;
	}

	public Integer getSegundoParametro() {
		return segundoParametro;
	}

	public void setSegundoParametro(Integer segundoParametro) {
		this.segundoParametro = segundoParametro;
	}

}
