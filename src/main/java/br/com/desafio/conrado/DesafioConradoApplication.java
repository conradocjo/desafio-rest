package br.com.desafio.conrado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioConradoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioConradoApplication.class, args);
	}

}
