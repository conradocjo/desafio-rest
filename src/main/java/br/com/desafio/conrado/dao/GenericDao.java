package br.com.desafio.conrado.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class GenericDao<T> {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Este método provê uma forma de interação com Banco usando JPA.
	 * */
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
}
