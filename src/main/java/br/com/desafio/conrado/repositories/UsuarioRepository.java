package br.com.desafio.conrado.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.desafio.conrado.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
