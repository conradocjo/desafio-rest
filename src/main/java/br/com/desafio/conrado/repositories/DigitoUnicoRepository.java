package br.com.desafio.conrado.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.desafio.conrado.model.DigitoUnico;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {
	
	@Query(value = " SELECT digitos FROM DigitoUnico digitos INNER JOIN digitos.usuario usuario WHERE usuario.id = :idUsuario ",nativeQuery = false)
	public List<DigitoUnico> buscarDigitoUnicoPorIdUsuario(@Param(value = "idUsuario") Long idUsuario);
	
}
