package br.com.desafio.conrado.services.impl;

import static br.com.desafio.conrado.controller.UsuarioController.retornaDigitoUnico;
import static br.com.desafio.conrado.controller.UsuarioController.verificaSeFoiPassadoParametroParaCalculo;
import static java.util.Objects.nonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.desafio.conrado.model.DigitoUnico;
import br.com.desafio.conrado.model.Usuario;
import br.com.desafio.conrado.repositories.DigitoUnicoRepository;
import br.com.desafio.conrado.repositories.UsuarioRepository;
import br.com.desafio.conrado.services.UsuarioService;
import br.com.desafio.conrado.utils.DataHolder;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private DigitoUnicoRepository digitoUnicoRepository;

	/**
	 * Realiza persistencia dos dados do usuário.
	 */
	@Override
	public Usuario gravarUsuario(Usuario usuario) {
		Usuario retorno = new Usuario();
		if (verificaSeFoiPassadoParametroParaCalculoAtravesDoCadastro(usuario)) {
			List<DigitoUnico> listaDigitoTemporaria = usuario.getListaResultadosDigitosUnicosCalculados();
			usuario.setListaResultadosDigitosUnicosCalculados(new ArrayList<>());
			usuario = this.usuarioRepository.save(usuario);
			for (DigitoUnico digitoUnico : listaDigitoTemporaria) {
				digitoUnico.setUsuario(usuario);
				digitoUnico.setDigitoUnico(retornaValorDigitoUnico(
						digitoUnico.getPrimeiroParametro().toString(), digitoUnico.getSegundoParametro().toString()));
			}
			digitoUnicoRepository.saveAll(listaDigitoTemporaria);
		} else {
			usuario.setListaResultadosDigitosUnicosCalculados(new ArrayList<>());
			retorno = this.usuarioRepository.save(usuario);
		}
		return retorno;
	}

	/**
	 * Verifica se foi passado parametros para calculo
	 */
	@Override
	public boolean verificaSeFoiPassadoParametroParaCalculoAtravesDoCadastro(Usuario usuario) {
		return verificaSeFoiPassadoParametroParaCalculo(usuario);
	}

	/**
	 * Atualiza dados do usuário, adicionando calculos que não existiam.
	 */
	@Override
	public void atualizarUsuario(Usuario usuarioRetornadoDoBanco, Usuario usuarioComDadosParaSerAtualizado) {
		Usuario usuario = atualizaDadosDoUsuario(usuarioRetornadoDoBanco, usuarioComDadosParaSerAtualizado);
		atualizaCalculosDoUsuario(usuario, usuarioComDadosParaSerAtualizado);
	}

	/**
	 * O método abaixo verifica se a lista de calculo do banco possui os calculos
	 * passado via parametro caso não exista os calculos serão adicionados à lista
	 * existente.
	 */
	private void atualizaCalculosDoUsuario(Usuario usuarioRetornadoDoBanco, Usuario usuarioComDadosParaSerAtualizado) {
		usuarioComDadosParaSerAtualizado.getListaResultadosDigitosUnicosCalculados().forEach(digito -> {
			String primeiroParametro = digito.getPrimeiroParametro() != null ? digito.getPrimeiroParametro().toString()
					: null;
			String segundoParametro = digito.getSegundoParametro() != null ? digito.getSegundoParametro().toString()
					: null;
			digito.setUsuario(usuarioRetornadoDoBanco);
			digito.setDigitoUnico(retornaValorDigitoUnico(primeiroParametro, segundoParametro));
		});
		this.digitoUnicoRepository
				.saveAll(usuarioComDadosParaSerAtualizado.getListaResultadosDigitosUnicosCalculados());
	}

	/**
	 * Este método auxiliar tem por objetivo a atualização de dados do usuário.
	 */
	private Usuario atualizaDadosDoUsuario(Usuario usuarioRetornadoDoBanco, Usuario usuarioComDadosParaSerAtualizado) {
		usuarioRetornadoDoBanco.setEmail(usuarioComDadosParaSerAtualizado.getEmail());
		usuarioRetornadoDoBanco.setNome(usuarioComDadosParaSerAtualizado.getNome());
		return this.usuarioRepository.save(usuarioRetornadoDoBanco);
	}

	/**
	 * Retorna calculo realizado referente ao digito único.
	 * Caso esteja entre os 10 calculos realizados, o cache será consultado ao invés do calculo ser feito novamente.
	 */
	@Override
	public Integer retornaValorDigitoUnico(String primeiroParametro, String segundoParametro) {
		Integer digitoUnico = null;
		Object teste = DataHolder.obter(primeiroParametro.toString().concat(segundoParametro));
		if (nonNull(teste)) {
			digitoUnico = (Integer) teste;
		} else {
			digitoUnico = realizaCalculoDigitoUnico(primeiroParametro, segundoParametro);
			DataHolder.guardar(primeiroParametro.toString().concat(segundoParametro), digitoUnico);
		}
		return digitoUnico;
	}
	
	@Override
	public Integer realizaCalculoDigitoUnico(String primeiroParametro, String segundoParametro) {
		return retornaDigitoUnico(primeiroParametro, segundoParametro);
	}

	/**
	 * Deleta Usuário e todos calculos realizados em cascata.
	 */
	@Override
	public void deletarUsuario(Usuario usuario) {
		this.usuarioRepository.delete(usuario);
	}

	/**
	 * Retorna todos usuários com calculos realizados.
	 */
	@Override
	public List<Usuario> retornarUsuarios() {
		return this.usuarioRepository.findAll();
	}

	/**
	 * Retorna usuário e cálculos realizados pelo id do usuário.
	 */
	@Override
	public Optional<Usuario> retornaUsuario(Long id) {
		return this.usuarioRepository.findById(id);
	}

}
