package br.com.desafio.conrado.services;

import java.util.List;

import br.com.desafio.conrado.model.DigitoUnico;

public interface DigitoUnicoService {

	void salvarCalculoDigitoUnico(List<DigitoUnico> digitoUnicos);

	List<DigitoUnico> buscarTodosCalculosDoUsuario(Long idUsuario);

	
}
