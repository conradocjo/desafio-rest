package br.com.desafio.conrado.services;

import java.util.List;
import java.util.Optional;

import br.com.desafio.conrado.model.Usuario;

public interface UsuarioService {

	/**
	 * Realiza persistencia dos dados do usuário.
	 */
	Usuario gravarUsuario(Usuario usuario);

	/**
	 * Verifica se foi passado parametros para calculo
	 */
	boolean verificaSeFoiPassadoParametroParaCalculoAtravesDoCadastro(Usuario usuario);

	/**
	 * Atualiza dados do usuário, adicionando calculos que não existiam.
	 */
	void atualizarUsuario(Usuario usuarioRetornadoDoBanco, Usuario usuarioComDadosParaSerAtualizado);

	/**
	 * Deleta Usuário e todos calculos realizados em cascata.
	 */
	void deletarUsuario(Usuario usuario);

	/**
	 * Retorna todos usuários com calculos realizados.
	 */
	List<Usuario> retornarUsuarios();

	/**
	 * Retorna usuário e cálculos realizados pelo id do usuário.
	 */
	Optional<Usuario> retornaUsuario(Long id);

	Integer realizaCalculoDigitoUnico(String primeiroParametro, String segundoParametro);

	/**
	 * Retorna calculo realizado referente ao digito único.
	 * Caso esteja entre os 10 calculos realizados, o cache será consultado ao invés do calculo ser feito novamente.
	 */
	Integer retornaValorDigitoUnico(String primeiroParametro, String segundoParametro);

}
