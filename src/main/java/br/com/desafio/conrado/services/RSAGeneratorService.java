package br.com.desafio.conrado.services;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Map;

import br.com.desafio.conrado.model.Usuario;

public interface RSAGeneratorService {

	/**
	 * Retorna mapa com a chave privada, e chave publica com base no usuário passado
	 * por parâmetro.
	 */
	Map<Usuario, Object> generatePublicAndPrivateKeys(Usuario usuario) throws NoSuchAlgorithmException;

	/**
	 * Criptografa Usuario passado por parâmetro.
	 */
	Usuario encryptUser(Usuario usuario, PublicKey publicKey) throws IOException, GeneralSecurityException;

	/**
	 * Método auxiliar para realizar criptografia, recebe por parâmetro, valor a ser
	 * encriptado e a chave pública.
	 */
	String encrypt(String valor, PublicKey chave) throws IOException, GeneralSecurityException;

	/**
	 * Descriptografa Usuário passado por parâmetro
	 */
	Usuario decryptUser(Usuario usuario, PrivateKey privateKey) throws IOException, GeneralSecurityException;

	/**
	 * Método auxiliar para descriptografar um valor, recebe por parâmetro valor que
	 * será descriptografado e a chave privada.
	 */
	String decrypt(String valor, PrivateKey chave) throws IOException, GeneralSecurityException;

}
