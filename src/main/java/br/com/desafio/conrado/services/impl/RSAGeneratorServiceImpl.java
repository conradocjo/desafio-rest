package br.com.desafio.conrado.services.impl;

import static br.com.desafio.conrado.utils.ResourcesUtils.ALGORITMO_CRIPTOGRAFIA;
import static br.com.desafio.conrado.utils.ResourcesUtils.UTF8;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

import br.com.desafio.conrado.model.Usuario;
import br.com.desafio.conrado.services.RSAGeneratorService;

@Service
public class RSAGeneratorServiceImpl implements RSAGeneratorService {

	private Map<Usuario, Object> keys = new HashMap<>();

	/**
	 * Retorna mapa com a chave privada, e chave publica com base no usuário passado
	 * por parâmetro.
	 */
	@Override
	public Map<Usuario, Object> generatePublicAndPrivateKeys(Usuario usuario) throws NoSuchAlgorithmException {
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITMO_CRIPTOGRAFIA);
		keyPairGenerator.initialize(2048);
		KeyPair chave = keyPairGenerator.generateKeyPair();
		keys.put(usuario, chave);
		return keys;
	}

	/**
	 * Criptografa Usuario passado por parâmetro.
	 */
	@Override
	public Usuario encryptUser(Usuario usuario, PublicKey publicKey) throws IOException, GeneralSecurityException {
		if (publicKey != null) {
			usuario.setNome(encrypt(usuario.getNome(), publicKey));
			usuario.setEmail(encrypt(usuario.getEmail(), publicKey));
		}
		return usuario;
	}

	/**
	 * Método auxiliar para realizar criptografia, recebe por parâmetro, valor a ser
	 * encriptado e a chave pública.
	 */
	@Override
	public String encrypt(String valor, PublicKey chave) throws IOException, GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(ALGORITMO_CRIPTOGRAFIA);
		cipher.init(Cipher.ENCRYPT_MODE, chave);
		return Base64.encodeBase64String(cipher.doFinal(valor.getBytes(UTF8)));
	}

	/**
	 * Descriptografa Usuário passado por parâmetro
	 */
	@Override
	public Usuario decryptUser(Usuario usuario, PrivateKey privateKey) throws IOException, GeneralSecurityException {
		if (privateKey != null) {
			usuario.setNome(decrypt(usuario.getNome(), privateKey));
			usuario.setEmail(decrypt(usuario.getEmail(), privateKey));
		}
		keys.remove(usuario);
		return usuario;
	}

	/**
	 * Método auxiliar para descriptografar um valor, recebe por parâmetro valor que
	 * será descriptografado e a chave privada.
	 */
	@Override
	public String decrypt(String valor, PrivateKey chave) throws IOException, GeneralSecurityException {
		Cipher cipher = Cipher.getInstance(ALGORITMO_CRIPTOGRAFIA);
		cipher.init(Cipher.DECRYPT_MODE, chave);
		return new String(cipher.doFinal(Base64.decodeBase64(valor)), UTF8);
	}
}
