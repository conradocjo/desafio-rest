package br.com.desafio.conrado.services.impl;

import static java.util.Objects.nonNull;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.desafio.conrado.dao.GenericDao;
import br.com.desafio.conrado.model.DigitoUnico;
import br.com.desafio.conrado.repositories.DigitoUnicoRepository;
import br.com.desafio.conrado.services.DigitoUnicoService;

@Service
public class DigitoUnicoServiceImpl extends GenericDao<DigitoUnico> implements DigitoUnicoService {

	@Autowired
	private DigitoUnicoRepository digitoUnicoRepository;

	@Override
	public void salvarCalculoDigitoUnico(List<DigitoUnico> digitoUnicos) {
		this.digitoUnicoRepository.saveAll(digitoUnicos);
	}

	@Override
	public List<DigitoUnico> buscarTodosCalculosDoUsuario(Long idUsuario) {
		List<DigitoUnico> digitosUnicos = new ArrayList<>();
		if (nonNull(idUsuario)) {
			digitosUnicos = this.digitoUnicoRepository.buscarDigitoUnicoPorIdUsuario(idUsuario);
		}
		return digitosUnicos;
	}
}
