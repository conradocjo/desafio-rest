package br.com.desafio.conrado.exceptions;

public class ObjectNotFound extends RuntimeException {

	private static final long serialVersionUID = 9092238553359483667L;
	
	/**
	 * Método personalizado para retorno do fluxo de exceção.
	 * */
	public ObjectNotFound(String msg) {
		super(msg);
	}

}
