package br.com.desafio.conrado.controller;

import static java.lang.Integer.parseInt;
import static java.util.Objects.nonNull;

import java.util.List;

import br.com.desafio.conrado.model.DigitoUnico;
import br.com.desafio.conrado.model.Usuario;

public class UsuarioController {

	/**
	 * A função digitoUnico deverá ter os seguintes parâmetros; 
	 * 1. n: uma string  * representado um inteiro. 1<=n<=10ˆ1000000 
	 * 2. k: um inteiro representando o * número de vezes da concatenação 1<=k<=10ˆ5 
	 * 3. A função digitoUnico deverá  obrigatoriamente retornar um inteiro.
	 */
	public static Integer retornaDigitoUnico(String primeiroParametro, String segundoParametro) {
		Integer digitoUnico = 0;
		if (isParamNonNull(segundoParametro)) {
			if (isParamNonNull(primeiroParametro)) {
				String valorConcatenado = realizaConcatenacaoDosParametros(primeiroParametro, parseInt(segundoParametro));
				digitoUnico = retornaSomaDosValores(valorConcatenado, digitoUnico);
			}
		} else {
			if (isParamNonNull(primeiroParametro) && verificaSeParametroEhMaiorQueUm(primeiroParametro)) {
				digitoUnico = retornaSomaDosValores(primeiroParametro, digitoUnico);
			} else if (isParamNonNull(primeiroParametro)) {
				digitoUnico = parseInt(primeiroParametro);
			}			
		}
		return digitoUnico;
	}

	/**
	 * Método verifica se foi passado parametros para calculo
	 * */
	public static boolean verificaSeFoiPassadoParametroParaCalculo(Usuario usuario) {
		return usuario.getListaResultadosDigitosUnicosCalculados() != null && listaDigitoUnicoIsNotEmpty(usuario.getListaResultadosDigitosUnicosCalculados()) ? true : false;
	}

	//Métodos Auxiliares.
	
	/**
	 * Realiza os calculos dos valores para dígito único.
	 * */
	private static Integer retornaSomaDosValores(String primeiroParametro, Integer digitoUnico) {
		for (int i = 0; i < primeiroParametro.length(); i++) {
			if (digitoUnico == 0) {
				digitoUnico = parseInt(Character.valueOf(primeiroParametro.charAt(i)).toString());
			} else {
				digitoUnico += parseInt(Character.valueOf(primeiroParametro.charAt(i)).toString());
			}
			
		}
		return digitoUnico;
	}
	
	/**
	 * Verifica se o tamanho do parametro é maior do que 1.
	 * */
	private static boolean verificaSeParametroEhMaiorQueUm(String primeiroParametro) {
		return primeiroParametro.length() > 1;
	}
	
	/**
	 * Verifica se parâmetro passado não é nulo.
	 * */
	private static boolean isParamNonNull(String param) {
		return nonNull(param) && param.length() > 0;
	}
	
	/**
	 * Dado dois números n e k, P deverá ser criado da concatenação da string n * k.
	 * */
	private static String realizaConcatenacaoDosParametros(String primeiroParametro, Integer segundoParametro) {
		StringBuilder parametrosConcatenados = new StringBuilder();
		for (int i = 0; i < segundoParametro; i++) {
			parametrosConcatenados.append(primeiroParametro);	
		}
		return parametrosConcatenados.toString();
	}
	
	/**
	 * Verifica se lista de digito unico não está vazia.
	 * */
	private static boolean listaDigitoUnicoIsNotEmpty(List<DigitoUnico> lista) {
		return !lista.isEmpty();
	}

}
