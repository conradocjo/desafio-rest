package br.com.desafio.conrado.utils;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataHolder {

	private static Map<String, WeakReference<Object>> dados = new HashMap<String, WeakReference<Object>>();

	/**
	 * Método responsável por guardar cache.
	 */
	public static void guardar(String id, Object object) {
		if (dados.size() > 9) {
			List<String> lista = dados.keySet().stream().collect(Collectors.toList());
			dados.remove(lista.get(0));
		}
		dados.put(id, new WeakReference<Object>(object));
	}

	/**
	 * Método responsável por retornar dados do cache.
	 */
	public static Object obter(String id) {
		WeakReference<Object> objectWeakReference = getDados().get(id);
		Object retorno = objectWeakReference != null ? objectWeakReference.get() : null;
		return retorno;
	}

	public static Map<String, WeakReference<Object>> getDados() {
		return dados;
	}

	public static void setDados(Map<String, WeakReference<Object>> dados) {
		DataHolder.dados = dados;
	}

}
