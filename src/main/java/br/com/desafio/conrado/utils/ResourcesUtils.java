package br.com.desafio.conrado.utils;

public class ResourcesUtils {

	//Mensagens
	
	public static String USUARIO_BUSCADO_NAO_ENCONTRADO = "O Usuário no qual está buscando não foi encontrado.";
	public static String USUARIO_A_SER_DELETADO_NAO_ENCONTRADO = "O Usuário que está tentando deletar não existe.";
	public static String USUARIO_A_SER_ATUALIZADO_NAO_ENCONTRADO = "O Usuário a ser atualizado não foi encontrado na base de dados.";

	//Criptografia
	
	public static final String ALGORITMO_CRIPTOGRAFIA = "RSA";
	public static final String UTF8 = "UTF-8";

}
