# Desafio Rest

Está API foi desenvolvida em Java, usando Java 8, Spring, Swagger, Maven, Mockito, Junit, etc...

Como compilar e executar a API?

- É bem simples, siga os passos abaixo:
	
1º Clone o código fonte para algum lugar do seu computador usando o comando:

git clone https://gitlab.com/conradocjo/desafio-rest.git

2º Certifique-se de que tenha o Maven instalado em seu computador e com a váriavel de ambiente configurada.
se tudo estiver certo execute o comando abaixo dentro da pasta do projeto usando o terminal da sua preferência:

mvn clean install -U

Obs: este comando já irá verificar se tem problemas relacionado ao teste unitário, irá ser realizado a validação do Maven direto
nos testes unitários da aplicação.	

Depois te ter executado o comando do maven irá ser gerado um arquivo arquivo ".jar", basta executa-lo com o comando "java -jar nomeDoArquivo.jar",
que irá subir a aplicação com todas as ferramentas integradas. 

Para acessar o Swagger:

http://localhost:8080/swagger-ui.html

Para executar o banco de dados:

http://localhost:8080/h2-console/
	
2. Se desejar executar os testes unitários, basta abrir a aplicação na IDE de sua preferência (eu usei o STS), importar o projeto 
maven, executar maven install, ou abrir a classe DesafioConradoTest.java, e executar usando JUnit.

Os testes unitários estão validando métodos relacionados à regra de negócio.
Eles estão com cobertura total da validação da regra de negócio, Utilizei o Eclema (plugin do Eclipse para este teste).

Foi adicionado a pasta raiz do projeto o arquivo "postman_collection.json", que possui os testes das API's.

As  tratativas referente ao cache estão na classe DataHolder e está sendo usado no método 
"realizaCalculoIhRetornaDigitoUnico(String primeiroParametro, String segundoParametro)" 
desta forma o cálculo só será realizado se não tiver no cache.








